FROM openjdk:12-alpine
ENV SPRING_PROFILE=h2
COPY target/assignment-*.jar /assignment.jar
CMD ["java", "-jar", "/assignment.jar"]
EXPOSE 8080
#ENTRYPOINT java -jar $h2 *.jar
